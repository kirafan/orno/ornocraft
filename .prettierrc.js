/** @format */

module.exports = {
  printWidth: 120,
  semi: true,
  singleQuote: true,
  traillingComma: 'all',
  bracketSpacing: true,
  jsxBracketSameLine: true,
  arrowParens: 'avoid',
  tabWidth: 2,
  useTabs: false
};
