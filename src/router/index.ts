import Vue from 'vue';
import VueRouter from 'vue-router';
import ViewHome from '../views/Home.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: ViewHome
  }
];

const router = new VueRouter({
  routes
});

export default router;
